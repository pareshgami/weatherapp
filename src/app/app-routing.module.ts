import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SearchComponent } from './templates/search/search.component';
import { WeatherComponent } from './templates/weather/weather.component';
import { HomeComponent } from './templates/home/home.component';

//routes
const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'search/:keyword', component: SearchComponent },
    { path: 'weather/:woeid', component: WeatherComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
