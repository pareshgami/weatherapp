import { Injectable } from '@angular/core';
import {Http, Response, Headers,RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';  

@Injectable()
export class DataServiceService {

	  http:Http;
	  baseUrl = 'http://127.0.0.1/angular/weather.php?command=';

  	constructor(http:Http) 
  	{ 
  		this.http = http;
  	}

  	getRequest(url,keyword)
  	{
      return this.http.get(this.baseUrl+url+keyword).map((res: Response) => res.json()); 
    }

}
