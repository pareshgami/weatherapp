import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

// ROUTING
import { AppRoutingModule } from './app-routing.module';

// COMPONENTS
import { AppComponent } from './app.component';
import { SearchComponent } from './templates/search/search.component';
import { WeatherComponent } from './templates/weather/weather.component';
import { HomeComponent } from './templates/home/home.component';
import { WeatherCustomComponent } from './components/weather-custom/weather-custom.component';

// SERVICES
import { DataServiceService } from './services/data-service.service';

//PIPES
import { RoundPipe } from './round.pipe';
import { DayConvertPipe } from './day-convert.pipe';


@NgModule({
  declarations: [
	AppComponent,
    SearchComponent,
    WeatherComponent,
    HomeComponent,
    WeatherCustomComponent,
    RoundPipe,
    DayConvertPipe
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule
  ],

  providers: [DataServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
