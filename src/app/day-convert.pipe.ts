import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dayConvert'
})
export class DayConvertPipe implements PipeTransform {

  transform(value:string)
  {
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	var d = new Date(value);
	var dayName = days[d.getDay()];
	return dayName;
  }

}
