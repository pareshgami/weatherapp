import { Component, OnInit, Input } from '@angular/core';
import { DataServiceService } from '../../services/data-service.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-weather-custom',
  templateUrl: './weather-custom.component.html',
  styleUrls: ['./weather-custom.component.css']
})
export class WeatherCustomComponent implements OnInit 
{

	@Input() cityname: string;
	@Input() woeid: string;
	data = [];	
	weatherStates = {
		"sn":
		{
			"name":"Snow",
			"icon":"https://www.metaweather.com/static/img/weather/sn.svg"
		},
		"sl":
		{
			"name":"Sleet",
			"icon":"https://www.metaweather.com/static/img/weather/sl.svg"
		},
		"h":
		{
			"name":"Hail",
			"icon":"https://www.metaweather.com/static/img/weather/h.svg"
		},
		"t":
		{
			"name":"Thunderstorm",
			"icon":"https://www.metaweather.com/static/img/weather/t.svg"
		},
		"hr":
		{
			"name":"Heavy Rain",
			"icon":"https://www.metaweather.com/static/img/weather/hr.svg"
		},
		"lr":
		{
			"name":"Light Rain",
			"icon":"https://www.metaweather.com/static/img/weather/lr.svg"
		},
		"s":
		{
			"name":"Showers",
			"icon":"https://www.metaweather.com/static/img/weather/s.svg"
		},
		"hc":
		{
			"name":"Heavy Cloud",
			"icon":"https://www.metaweather.com/static/img/weather/hc.svg"
		},
		"lc":
		{
			"name":"Light Cloud",
			"icon":"https://www.metaweather.com/static/img/weather/lc.svg"
		},
		"c":
		{
			"name":"Clear",
			"icon":"https://www.metaweather.com/static/img/weather/c.svg"
		}
	}
	noResultFound = false;
	showLoader = true;
	detailPage = false;
	

  	constructor(private service:DataServiceService,
  				private router:Router) 
  	{ 
  		
  	}

  	ngOnInit() 
  	{
  		this.noResultFound = false;
  		if(this.cityname != undefined)
  		{
  			this.detailPage = false;
  			this.service.getRequest('search&keyword=',this.cityname).
		  	subscribe(res => 
		  	{
		  		if(res.length > 0)
		  		{
		  			for(var i = 0; i < res.length; i++)
			    	{
			    		this.service.getRequest('location&woeid=',res[i].woeid).
			  			subscribe(resData => 
			  			{
			  				this.data = resData
			  				this.noResultFound = false;
			  				this.showLoader = false;
			  			});
			    	}
			    }
			    else
			    {
			    	this.noResultFound = true;
			    	this.showLoader = false;
			    }	  
		  	});	
  		}
  		else
  		{
  			this.detailPage = true;
  			this.noResultFound = false;
  			this.service.getRequest('location&woeid=',this.woeid).
  			subscribe(resData => 
  			{
  				this.data = resData
  				this.showLoader = false;
  			});
  		}
  		
  	}

  	weatherDetails(woeid)
  	{
  		this.router.navigate(['weather/'+woeid]);
  	}

}
