import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

	cityname = '';
  	constructor(private router:Router,
  				private route: ActivatedRoute) 
  	{ 
  		this.cityname = this.route.params['_value']['keyword'];
  	}

  	ngOnInit() 
  	{

  	}

  	searchByCity(serachKeyword)
    {
        this.router.navigateByUrl('/home', { skipLocationChange: true });
        setTimeout(()=>this.router.navigate(['search/'+serachKeyword]));
    }

}
