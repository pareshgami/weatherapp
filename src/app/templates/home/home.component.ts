import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    cityNames = [
        "Istanbul",
        "Berlin",
        "London",
        "Helsinki",
        "Dublin",
        "Vancouver"
    ]
    search = '';

    constructor(private router:Router) 
    {

    }

    ngOnInit() 
    {

    }

    searchByCity(serachKeyword)
    {
        this.router.navigate(['search/'+serachKeyword]);
    }

}
