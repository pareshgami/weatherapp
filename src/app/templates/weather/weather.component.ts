import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

	woeid = '';
    constructor(private router:Router,
  				private route: ActivatedRoute) 
    { 
    	this.woeid = this.route.params['_value']['woeid'];
    }

  	ngOnInit() 
  	{

  	}

}
